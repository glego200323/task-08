package com.epam.rd.java.basic.task8.entity;
import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class Flower {
    private String name;
    private String soil;
    private String origin;
    private VisualParameters visualParameters;
    private GrowingTips growingTips;
    private String multiplying;
}
@Getter
@Setter
class VisualParameters {
    private String stemColour;
    private String leafColour;
    private Integer aveLenFlower;
}
@Getter
@Setter
class GrowingTips {
    private Integer tempreture;
    private Boolean lighting;
    private Integer watering;
}
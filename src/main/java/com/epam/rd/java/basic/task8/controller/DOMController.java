package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.entity.Flower;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE

	public List<Flower> readXML(){
		List<Flower> flowers = new ArrayList<>();
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		Document doc;
		try {
			db = dbf.newDocumentBuilder();
			doc = db.parse(new File(xmlFileName));
			NodeList nodeList = doc.getElementsByTagName("flower");


		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}
		return flowers;
	}
}
